# Ame Star Wars challenge

On this project I have used those frameworks:

* [Spring Boot](https://docs.spring.io/spring-boot/docs/{bootVersion}/reference/htmlsingle/#production-ready)
* [Spring Data JPA](https://docs.spring.io/spring-boot/docs/{bootVersion}/reference/htmlsingle/#boot-features-jpa-and-spring-data)
* [Flyway Migration](https://docs.spring.io/spring-boot/docs/{bootVersion}/reference/htmlsingle/#howto-execute-flyway-database-migrations-on-startup)

### Comments
I have decided to build an extremely simple application to keep it simple to test and read.

On this project you'll find a postman file to call the APIs.

All APIs are assured by integration test

Reactive: would be nice to use reactive or coroutine on find all API since it take a time a little longer than others and it might block a thread, on real applications on larger amount of data I would have developed a paginated API
or Spring Webflux to stream the data. 

