package com.ame.challenge.integration

import com.ame.challenge.repository.PlanetRepository
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import junit.framework.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.MvcResult
import org.springframework.test.web.servlet.ResultActions
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

@RunWith(SpringRunner::class)
@SpringBootTest
@AutoConfigureMockMvc
class PlanetApi {

    private companion object {
        const val INSERT_REQUEST = """
            {
                "name": "Yavin IV",
                "climate":"polar",
                "terrain":"grass"
            }"""
    }

    @Autowired
    lateinit var mvc: MockMvc

    @Autowired
    lateinit var planetRepository: PlanetRepository

    @Test
    fun `assert planet results size is the same on Swapi`() {
        mvc.perform(get("/planet/external"))
            .andExpect(status().isOk)
            .andExpect {
                assertEquals(ObjectMapper().readValue<JsonNode>(it.response.contentAsString).get("planets").size(), 61)
            }
    }

    @Test
    fun `Insert planet then validate`() {
        insertPlanet().andExpect(status().isAccepted)

        planetRepository.findAll()
            .run {
                assertEquals(this.size, 1)
                assertEquals(this[0].filmsAmount, 1)
            }

        planetRepository.findByName("Yavin IV").get().run {
            assertEquals(this.climate, "polar")
            assertEquals(this.terrain, "grass")
        }
    }

    @Test
    fun `Assert delete`() {
        val id = extractId(insertPlanet().andReturn())

        mvc.perform(delete("/planet/$id"))
            .andExpect(status().isAccepted)

        assertEquals(planetRepository.findAll().size, 0)
    }

    @Test
    fun `Find by name and ID`() {
        val id = extractId(insertPlanet().andReturn())

        mvc.perform(get("/planet/Yavin IV"))
            .andExpect(status().isOk)

        mvc.perform(get("/planet/$id"))
            .andExpect(status().isOk)

        mvc.perform(get("/planet/NoContent"))
            .andExpect(status().isNoContent)

    }

    private fun extractId(result: MvcResult): String {
        return ObjectMapper().readValue<JsonNode>(result.response.contentAsString)
            .get("url").asText()
            .split("/planet/")[1]
    }

    private fun insertPlanet(): ResultActions {
        return mvc.perform(post("/planet")
            .contentType(MediaType.APPLICATION_JSON)
            .content(INSERT_REQUEST)
        )
    }

}