CREATE TABLE planet
(
    id                  UUID  NOT NULL,
    name                varchar(200),
    terrain             varchar(200)  NULL,
    climate             varchar(200) NULL,
    films_amount        INT NOT NULL,
    CONSTRAINT id_pkey PRIMARY KEY (id)
)
