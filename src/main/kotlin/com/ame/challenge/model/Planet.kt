package com.ame.challenge.model

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import java.util.UUID
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
@Table(name = "planet")
data class Planet (
    @Id
    @Column(name = "id")
    val id: UUID = UUID.randomUUID(),

    @Column(name = "name")
    val name: String,

    @Column(name = "climate")
    val climate: String,

    @Column(name = "terrain")
    val terrain: String,

    @Column(name = "films_amount")
    val filmsAmount: Int
)
