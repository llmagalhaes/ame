package com.ame.challenge.integration

import com.ame.challenge.controller.response.StarwarsPlanet
import com.ame.challenge.controller.response.SwapiPlanetDetail
import com.ame.challenge.controller.response.SwapiPlanetResponse
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.web.client.HttpClientErrorException
import org.springframework.web.client.RestTemplate
import org.springframework.web.client.getForObject

@Service
class SwapiIntegration {

    @Autowired
    private lateinit var starWarsRestTemplate: RestTemplate

    private companion object {
        const val FIND_ALL_URI = "/planets"
        const val FIND_BY_NAME = "/planets/?search={name}"
        val LOGGER: Logger = LoggerFactory.getLogger(this::class.java)
    }

    fun findAll(): List<StarwarsPlanet> {
        val planetList = mutableListOf<SwapiPlanetDetail>()
        try {
            var planetResponse = starWarsRestTemplate.getForObject<SwapiPlanetResponse>(FIND_ALL_URI)!!
            planetList.addAll(planetResponse.results)

            while (planetResponse.next != null) {
                planetResponse = starWarsRestTemplate.getForObject(planetResponse.next!!)!!
                planetList.addAll(planetResponse.results)
            }
        } catch (e: HttpClientErrorException.NotFound) {
            LOGGER.error("Failed to fetch planet on SWAPI")
        }
        return planetList.map { StarwarsPlanet(it.name, it.climate, it.terrain, it.films.size) }
    }

    fun findByName(planetName: String): StarwarsPlanet? {
        return try {
            starWarsRestTemplate.getForObject<SwapiPlanetResponse>(FIND_BY_NAME, mapOf("name" to planetName))!!
                .results
                .first()
                .let { StarwarsPlanet(it.name, it.climate, it.terrain, it.films.size) }
        } catch (e: HttpClientErrorException.NotFound) {
            null
        }
    }

}
