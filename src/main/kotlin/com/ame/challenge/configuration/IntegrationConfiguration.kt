package com.ame.challenge.configuration

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.web.client.RestTemplateBuilder
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.client.ClientHttpRequestInterceptor
import org.springframework.web.client.RestTemplate


@Configuration
class IntegrationConfiguration {

    @Autowired
    private lateinit var restTemplateBuilder: RestTemplateBuilder

    @Value("\${starwars-api-uri}")
    private lateinit var starWarsApiUri: String

    @Bean
    fun starWarsRestTemplate(): RestTemplate = restTemplateBuilder
        .rootUri(starWarsApiUri)
        .interceptors(ClientHttpRequestInterceptor { request, body, execution ->
            request.headers.set("User-Agent", "Mozilla/5.0")
            execution.execute(request, body)
        }).build()

}