package com.ame.challenge.extension

import java.util.UUID

fun String.isUUID() : Boolean = try {
    this.toUUID()
    true
}catch (e: IllegalArgumentException) { false }

fun String.toUUID(): UUID = UUID.fromString(this)