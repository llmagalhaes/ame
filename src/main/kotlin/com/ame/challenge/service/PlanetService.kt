package com.ame.challenge.service

import com.ame.challenge.controller.response.StarwarsPlanet
import com.ame.challenge.exception.PlanetNotFoundException
import com.ame.challenge.integration.SwapiIntegration
import com.ame.challenge.model.Planet
import com.ame.challenge.repository.PlanetRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.UUID

@Service
class PlanetService {

    @Autowired
    private lateinit var planetRepository: PlanetRepository

    @Autowired
    private lateinit var swapiIntegration: SwapiIntegration

    fun findAll(lookOnExternal: Boolean = false): List<StarwarsPlanet> {
        return if (lookOnExternal) {
            swapiIntegration.findAll()
        } else {
            planetRepository.findAll().map { StarwarsPlanet(it.name, it.climate, it.terrain, it.filmsAmount) }
        }
    }

    fun find(id: UUID): StarwarsPlanet? {
        return planetRepository.findById(id)
            .map { StarwarsPlanet(it.name, it.climate, it.terrain, it.filmsAmount) }
            .orElse(null)
    }

    fun findByName(name: String): StarwarsPlanet? {
        return planetRepository.findByName(name)
            .map { StarwarsPlanet(it.name, it.climate, it.terrain, it.filmsAmount) }
            .orElse(null)
    }

    fun delete(id: UUID): Boolean {
        return try {
            planetRepository.deleteById(id)
            true
        } catch (e: Exception) {
            false
        }
    }

    fun save(planet: StarwarsPlanet): Planet {
        return swapiIntegration.findByName(planet.name)
            ?.let { planetRepository.save(planet.copy(filmAmount = it.filmAmount).toModel()) }
            ?: throw PlanetNotFoundException("There's no planet named ${planet.name}")
    }

}
