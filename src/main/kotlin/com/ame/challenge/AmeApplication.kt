package com.ame.challenge

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class AmeApplication

fun main(args: Array<String>) {
	runApplication<AmeApplication>(*args)
}
