package com.ame.challenge.exception

class PlanetNotFoundException(message: String?) : RuntimeException(message)
