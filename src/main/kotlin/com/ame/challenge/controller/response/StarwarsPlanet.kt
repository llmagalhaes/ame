package com.ame.challenge.controller.response

import com.ame.challenge.model.Planet
import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
data class StarwarsPlanet(
    val name: String,
    val climate: String,
    val terrain: String,
    val filmAmount: Int
) {
    fun toModel(): Planet {
        return Planet(
            name = this.name,
            climate = this.climate,
            terrain = this.terrain,
            filmsAmount = this.filmAmount
        )
    }
}