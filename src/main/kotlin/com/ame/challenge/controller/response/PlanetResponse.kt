package com.ame.challenge.controller.response

import com.fasterxml.jackson.annotation.JsonInclude

@JsonInclude(JsonInclude.Include.NON_NULL)
data class PlanetResponse(
    val planets: List<StarwarsPlanet>? = null,
    val planet: StarwarsPlanet? = null,
    val url: String? = null
)
