package com.ame.challenge.controller.response

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
data class SwapiPlanetResponse (
    val next: String?,
    val results: List<SwapiPlanetDetail>
)

data class SwapiPlanetDetail(
    val name: String,
    val climate: String,
    val terrain: String,
    val films: List<String> = listOf()
)