package com.ame.challenge.controller

import com.ame.challenge.controller.response.PlanetResponse
import com.ame.challenge.controller.response.StarwarsPlanet
import com.ame.challenge.exception.PlanetNotFoundException
import com.ame.challenge.extension.isUUID
import com.ame.challenge.extension.toUUID
import com.ame.challenge.service.PlanetService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.util.UUID

@RestController
@RequestMapping("/planet")
class PlanetController {

    private companion object {
        const val FIND_PLANET_URI = "http://localhost:8080/planet/%s"
    }

    @Autowired
    private lateinit var planetService: PlanetService

    @GetMapping("", "/{param}")
    fun listPlanets(@PathVariable(required = false) param: String?): ResponseEntity<PlanetResponse> {
        return when (param?.isUUID()) {
            true -> {
                planetService.find(param.toUUID())
                    ?.let { ResponseEntity.ok(PlanetResponse(planet = it)) }
                    ?: ResponseEntity.noContent().build()
            }

            false -> {
                planetService.findByName(param)
                    ?.let { ResponseEntity.ok(PlanetResponse(planet = it)) }
                    ?: ResponseEntity.noContent().build()
            }

            null -> {
                planetService.findAll()
                    .takeIf { it.isNotEmpty() }
                    ?.let { ResponseEntity.ok(PlanetResponse(it)) }
                    ?: ResponseEntity.noContent().build()
            }
        }

    }

    @GetMapping("/external")
    fun listPlanetsByApi(): ResponseEntity<PlanetResponse> = planetService.findAll(true)
        .takeIf { it.isNotEmpty() }
        ?.let { ResponseEntity.ok(PlanetResponse(it)) }
        ?: ResponseEntity.noContent().build()


    @PostMapping
    fun insert(@RequestBody planet: StarwarsPlanet): ResponseEntity<PlanetResponse> {
        return try {
            val planetSaved = planetService.save(planet)
            ResponseEntity
                .accepted()
                .body(PlanetResponse(
                    url = buildPlanetUrl(planetSaved.id),
                    planet = StarwarsPlanet(planetSaved.name,
                        planetSaved.climate,
                        planetSaved.terrain,
                        planetSaved.filmsAmount)))
        } catch (e: PlanetNotFoundException) {
            throw e
        }
    }

    @DeleteMapping("{id}")
    fun deleteById(@PathVariable id: UUID): ResponseEntity<Void> {
        return if (planetService.delete(id)) {
            ResponseEntity.accepted().build()
        } else {
            ResponseEntity.badRequest().build()
        }

    }

    private fun buildPlanetUrl(id: UUID) = FIND_PLANET_URI.format(id)
}
