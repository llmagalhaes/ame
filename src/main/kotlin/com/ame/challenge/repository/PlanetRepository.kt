package com.ame.challenge.repository

import com.ame.challenge.model.Planet
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.util.Optional
import java.util.UUID

@Repository
interface PlanetRepository: JpaRepository<Planet, UUID> {
    fun findByName(name: String): Optional<Planet>
}
